#include "schedule.hpp"

#include <algorithm>
#include <assert.h>
#include <fstream>
#include <utility>
#include <vector>

namespace schedule {

using std::make_pair;

static std::vector<std::pair<semester, std::string>> sem_vec{
    make_pair(FIRST, "first"),
    make_pair(SUMMER_EARLY, "summer-early"),
    make_pair(WINTER_EARLY, "winter-early"),
    make_pair(SUMMER_LATE, "summer-late"),
    make_pair(WINTER_LATE, "winter-late"),
    make_pair(SECOND, "second"),
    make_pair(THIRD, "third"),
    make_pair(THIRD_FIRST, "third-first")};

std::string to_string(semester sem) {
  auto it = std::find_if(sem_vec.begin(), sem_vec.end(),
                         [sem](auto &v) { return v.first == sem; });
  assert(it != sem_vec.end());
  return it->second;
}

std::optional<semester> from_string(const std::string &str) {
  auto it = std::find_if(sem_vec.begin(), sem_vec.end(),
                         [&str](auto &v) { return v.second == str; });
  if (it == sem_vec.end())
    return std::optional<semester>();
  else
    return it->first;
}

std::map<std::string, module>
read_schedule(const std::filesystem::path &filename) {
  std::ifstream is(filename);
  YAML::Node yaml = YAML::Load(is);
  return yaml.as<std::map<std::string, module>>();
}

std::ostream &operator<<(std::ostream &out, const module &m) {
  out << m.name << " ECTS:" << m.ects << " W:" << m.lectures
      << " Ć:" << m.exercises << " L:" << m.lab << " P:" << m.project;
  return out;
}

} // namespace schedule
