#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <map>
#include <optional>
#include <string>
#include <filesystem>

#include <yaml-cpp/yaml.h>
namespace schedule {

enum semester {
  FIRST,
  SUMMER_EARLY,
  WINTER_EARLY,
  SUMMER_LATE,
  WINTER_LATE,
  SECOND,
  THIRD,
  THIRD_FIRST
};

std::string to_string(semester sem);
std::optional<semester> from_string(const std::string &str);

struct module {
  std::string name;
  int ects;
  int lectures;
  int exercises;
  int lab;
  int project;
  semester sem;
};

std::map<std::string, module> read_schedule(const std::filesystem::path &filename);

std::ostream& operator << (std::ostream&out, const module& m);

} // namespace schedule

namespace YAML {
template <> struct convert<schedule::module> {
  static Node encode(const schedule::module &mod) {
    Node node;
    node["name"] = mod.name;
    node["ects"] = mod.ects;
    node["lec"] = mod.lectures;
    node["exc"] = mod.exercises;
    node["lab"] = mod.lab;
    node["pro"] = mod.project;
    node["sem"] = schedule::to_string(mod.sem);
    return node;
  }
  static bool decode(const Node &node, schedule::module &mod) {
    if (!node.IsMap())
      return false;
    mod.name = node["name"].as<std::string>();
    mod.ects = node["ects"].as<int>();
    mod.lectures = node["lec"].as<int>(0);
    mod.exercises = node["exc"].as<int>(0);
    mod.lab = node["lab"].as<int>(0);
    mod.project = node["pro"].as<int>(0);
    mod.sem = *schedule::from_string(node["sem"].as<std::string>());
    return true;
  }
};
} // namespace YAML

#endif /* SCHEDULE_H */
