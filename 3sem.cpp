#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include <assert.h>

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include "schedule.hpp"

using namespace schedule;
using std::string;

namespace schedule::formatter {

class formatter {
public:
  virtual void format_header(int sem_num, const std::string &s) = 0;
  virtual void format_entry(const module &m) = 0;
  virtual void format_summary(int ects, int hrs) = 0;
};

class standard_formatter : public formatter {
private:
  std::ostream &out;

public:
  standard_formatter(std::ostream &out) : out(out) {}

  virtual void format_header(int sem_num, const std::string &s) {
    out << "Semestr " << sem_num << "(" << s << ")"
        << "\n";
  }
  virtual void format_entry(const module &m) { out << m << std::endl; }
  virtual void format_summary(int ects, int hrs) {
    std::cout << "Godzin: " << hrs << " [22,25] "
              << "ECTS " << ects << "\n\n";
  }
};

class csv_formatter : public formatter {
  std::ofstream output;

public:
  csv_formatter(const std::filesystem::path &path) : output(path) {
    output << "Nazwa;ECTS;Wyk;Ćw;Lab;Pro" << std::endl;
  }
  virtual void format_header(int sem_num, const std::string &s) {
    output << "Semestr " << sem_num << "(" << s << ")"
           << "\n";
  };
  virtual void format_entry(const module &m) {
    output << m.name << ";" << m.ects << ";" << m.lectures << ";" << m.exercises
           << ";" << m.lab << ";" << m.project << std::endl;
  };
  virtual void format_summary(int ects, int hrs) {
    output << "ECTS: " << ects << ";Godzin: " << hrs << std::endl << std::endl;
  };
};

} // namespace schedule::formatter

int sem_number(semester sem, bool start_in_winter) {
  switch (sem) {
  case FIRST:
    return 0;
  case SECOND:
    return 1;
  case THIRD:
    return 2;
  case SUMMER_EARLY:
    return start_in_winter ? 1 : 0;
  case SUMMER_LATE:
    return start_in_winter ? 1 : 2;
  case WINTER_EARLY:
    return start_in_winter ? 0 : 1;
  case WINTER_LATE:
    return start_in_winter ? 2 : 1;
  case THIRD_FIRST:
    return start_in_winter ? 0 : 2;
  }
  assert(false);
  return -1;
}

std::string sem_season(int sem, bool start_in_winter) {
  return sem % 2 == (!start_in_winter) ? "zima" : "lato";
}

void process_program(const std::map<string, module> &modules,
                     bool start_in_winter,
                     schedule::formatter::formatter &formatter) {
  std::vector<std::vector<module>> sems;
  for (int i = 0; i < 3; i++) {
    sems.push_back(std::vector<module>());
  }
  for (auto &p : modules) {
    sems[sem_number(p.second.sem, start_in_winter)].push_back(p.second);
  }
  // TODO sumy, wypisanie

  int ects[3];
  int godziny[3];
  for (int i = 0; i < 3; i++) {
    ects[i] = 0;
    godziny[i] = 0;

    formatter.format_header(i + 1, sem_season(i, start_in_winter));

    for (auto &x : sems[i]) {
      ects[i] += x.ects;
      godziny[i] += x.exercises + x.lab + x.lectures + x.project;
      formatter.format_entry(x);
    }

    formatter.format_summary(ects[i], godziny[i]);
  }
}

int main(int argc, char **argv) {
  namespace po = boost::program_options;

  std::string input_file;
  po::variables_map vm;
  po::options_description desc("Options");
  po::positional_options_description pdesc;
  boost::optional<std::string> csv_output;

  pdesc.add("modules-yaml", 1);
  desc.add_options()("csv-output,c", po::value(&csv_output))(
      "modules-yaml", po::value(&input_file))("help", "produce help message");
  po::store(
      po::command_line_parser(argc, argv).options(desc).positional(pdesc).run(),
      vm);
  vm.notify();

  if (vm.count("help") > 0 || vm.count("modules-yaml") != 1) {
    std::cout << desc << std::endl;
    return 1;
  }
  std::unique_ptr<schedule::formatter::formatter> formatter(
      csv_output ? static_cast<schedule::formatter::formatter *>(
                       new schedule::formatter::csv_formatter(*csv_output))
                 : new schedule::formatter::standard_formatter(std::cout));

  auto modules = read_schedule(vm["modules-yaml"].as<std::string>());
  process_program(modules, false, *formatter);
  process_program(modules, true, *formatter);
}
